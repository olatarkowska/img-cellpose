FROM nvidia/cuda:10.2-cudnn8-runtime-ubuntu18.04


ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH
ENV LD_LIBRARY_PATH /usr/local/cuda-10.2/compat/${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

RUN apt-get update --fix-missing && \
    apt-get install -y \
        wget \
        bzip2 \
        ca-certificates \
        libglib2.0-0 \
        libxext6 \
        libsm6 \
        libxrender1 \
        git \
        mercurial \
        subversion \
        curl \
        grep \
        sed \
        dpkg && \

    wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-py38_4.8.2-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    find /opt/conda/ -follow -type f -name '*.a' -delete && \
    find /opt/conda/ -follow -type f -name '*.js.map' -delete && \
    /opt/conda/bin/conda clean -afy && \
    echo "conda activate base" >> ~/.bashrc && \

    TINI_VERSION=`curl https://github.com/krallin/tini/releases/latest | grep -o "/v.*\"" | sed 's:^..\(.*\).$:\1:'` && \
    curl -L "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini_${TINI_VERSION}.deb" > tini.deb && \
    dpkg -i tini.deb && \
    rm tini.deb && \

    pip install cellpose && \
    pip uninstall mxnet-mkl -y && \
    pip install mxnet-cu102mkl && \
    pip install scikit-image && \
    pip install --no-deps cellpose --upgrade && \

    apt-get install libgl1-mesa-glx -y && \

    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \

    /opt/conda/bin/conda update --all && \
    /opt/conda/bin/conda upgrade --all && \
    /opt/conda/bin/conda install \
        numpy \
        tifffile \
        cudatoolkit=10.2 && \
    /opt/conda/bin/conda clean -tipsy && \
    /opt/conda/bin/conda clean -afy